package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
