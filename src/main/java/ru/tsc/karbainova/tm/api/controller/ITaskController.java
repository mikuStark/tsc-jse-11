package ru.tsc.karbainova.tm.api.controller;

public interface ITaskController {
    void showTask();

    void clearTask();

    void createTask();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateByIndex();

    void updateById();
}
