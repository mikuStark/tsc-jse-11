package ru.tsc.karbainova.tm.controller;

import ru.tsc.karbainova.tm.api.controller.IProjectController;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectServices;

    public ProjectController(final IProjectService projectServices) {
        this.projectServices = projectServices;
    }

    @Override
    public void showProject() {
        final List<Project> projects = projectServices.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        projectServices.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        projectServices.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectServices.findById(id);
        if (project == null) {
            System.out.println("Project not find");
            return;
        }
        System.out.println(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectServices.findByIndex(index);
        if (project == null) {
            System.out.println("Project not find");
            return;
        }
        System.out.println(project);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectServices.findByName(name);
        if (project == null) {
            System.out.println("Project not find");
            return;
        }
        System.out.println(project);
    }

    @Override
    public void removeById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectServices.removeById(id);
        if (project == null) {
            System.out.println("Project not find");
        }
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectServices.removeByName(name);
        if (project == null) {
            System.out.println("Project not find");
        }
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectServices.removeByIndex(index);
        if (project == null) {
            System.out.println("Project not find");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectServices.findByIndex(index);
        if (project == null) {
            System.out.println("Project not find");
            return;
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectServices.updateByIndex(index, name, description);
        if (projectUpdate == null) System.out.println("Project not find");
    }

    @Override
    public void updateById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectServices.findById(id);
        if (project == null) {
            System.out.println("Project not find");
            return;
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectServices.updateById(id, name, description);
        if (projectUpdate == null) System.out.println("Project not find");
    }
}
